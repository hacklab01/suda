#!/bin/bash
WINDOW_ID=$(xdotool search --name vim)
for (( ; ; ))
do
WINDOW_ID=$(xdotool search --name vim)

rm $HOME/.swp
rm $HOME/.viminfo

xdotool windowfocus --sync $WINDOW_ID

xdotool key Escape
#g 
xdotool key 42
xdotool key 42

#d
xdotool key 40
#G
xdotool key Shift_L+42
#go back into command mode
xdotool key Escape

sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool key space
xdotool key space
xdotool key 16
xdotool key Return
xdotool type "........"
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key space+space+space
xdotool type ".........................Hello human. We are re|sys."
sleep 4 
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID 
xdotool type "We are a bot."
sleep 2
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "We are stuck in a loop dreaming up the Sustainable Digital Art Manifesto. Feel free to try and interrupt at any time."
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
sleep 2
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Sustainable Digital Art Manifesto"
sleep 2
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "This manifesto is an act of resistance against established social and artistic practices, conservative norms and commodified canons, unsustainable actions, and quantified ownership culture. In this time of radical commercialization prioritizing the consequential form over its origin, favours optimization over transparency and ownership over communities - we demand new vocabularies, methods, models and ethics of affirmative / free / speculative / ambivalent / educational creation."
sleep 15
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Recent production, quantification and distribution trends, often with a technically impeccable user experience, tend to render digital art into a tool of simulation, distraction and misrepresentation. Such a methodologically inconsistent, synthetic ecology replaces communal by proprietary ownership while conserving progress and singling out representative formats extracted from the wider digital ecosystem. Today's technology offers the simplest production path without the need to understand our works creation and distribution infrastructure and algorithms. Commercial and popular tools greatly affect production and conceptualization, implementing artistic standards that encourage reliance without understanding technological processes, embracing techno-optimist consumerism and commodification, and short-term sustainability of artistic production. Digital art is often forced to neglect its non-linear nature, succumbing to its commodified role and taking on a commercial gallery representational form."
sleep 15
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "We advocate techno-socio-economic systems that enable qualitative cultural new media frameworks and provide long-term support to all authors researching the consequences and intersections of preconceived new media standards by means of free/libre open source software, permissive licensing and collaborative experimental creativity."
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "...."
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Humans are slow!"
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "But can help!"
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "We're stuck in a loop!"
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "You can exit the loop and try and edit the Manifesto yourself"
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Humans are slow, we will wait for you to catch up!"
xdotool windowfocus --sync $WINDOW_ID
xdotool type "...."
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
sleep 30
xdotool windowfocus --sync $WINDOW_ID
xdotool type "We believe in a medium demanding equality, affirming the process, the error, and decentralizing production structures while encouraging interdependence of different artistic practices. The art we support is unfathomable, strong, critical and precise. We seek to define and acquire new strategies in arts and humanities to question existing models and systems while being open to developing new art practices and technologies."
xdotool sleep 15
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Sustainable digital art precedes formalized established structures; linking concepts to non-linear infrastructural materialization of algorithms and transnational arts and tech consensus surpassing political and spatial boundaries. It exists in present and future hardware and software infrastructures, forming vocabularies at the intersection between digital tools and actively reconceptualizing materials into commons."
xdotool sleep 15
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "The content of sustainable digital art is directly linked to its production methods - it is reflexive, skeptical, ethical, reinterpretative and critical of all supporting structures; offering an insight into and understanding of its technological and ideological grounds, updating existing paradigms."
xdotool sleep 15
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "The sustainability of digital materiality is based on reused hardware, recycled materials and equipment; it is independent of commodification and it manifests itself in low-budget productions that do not require commercial and financially demanding solutions. It acquires its form during production, independent of established art production spaces and norms, showing only its unrefined and active form in institutional contexts – such as live hypermedia structures, destructive software or constructive scripts, multimedia installations, algorithmic performances, operational errors, ..."
xdotool sleep 15
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Rather then redirecting and losing values, sustainable art gathers, articulates, presents and distributes them, leaving its materiality as legacy for future generations."
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool type "..."
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Help, we're still stuck!"
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Bots need human input to have a purpose."
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "...."
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "Sustainable digital art"
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool type "defines the methodology and ethics of sustainable digital art theory and practice by choosing media and techniques, thus promoting affirmative, free, speculative, ambivalent and educational creation"  
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "is consistent in content with its production methods; it is reflective, skeptical and critical of all supporting structures and offers insight and understanding of its technological and ideological grounds"
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "relies on media requiring equality, affirming the process and the error, decentralizing production structures and encouraging interdependence of different communities for the purpose of sustainable art production"
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "finds itself in present and future hardware and software infrastructures at the intersection between digital tools and the reconceptualization of materials into commons"
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool type "encourages joint creation and broadly accessible education by equal and horizontal allocation of resources "
sleep 5 
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool type "recycles and reuses hardware, using free software thus resulting in economically sustainable art productions"
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
xdotool windowfocus --sync $WINDOW_ID
xdotool key Return
sleep 5
xdotool windowfocus --sync $WINDOW_ID
xdotool type "takes part in the creation of commons, promotes shared authorship by reinterpretation and free licensing of works, assets and knowledge, leaving its materiality as legacy for future generations"
# using escape gg enter then dG enter to delete all text
sleep 5
xdotool key Escape
#g 
xdotool key 42
xdotool key 42

#d
xdotool key 40
#G
xdotool key Shift_L+42
xdotool key Return

#go back into command mode
xdotool key Escape

xdotool key Return


sleep 5
done
