!!!THIS REPO IS CURRENTLY 20% BROKEN!!!

The Rathole server setup for tunneling is currently broken. Please see documentation to setup manually. 

Prerequisites:

- a working Arch or Debian based distro with xorg (Tested on Trisquel, PureOS, Parabola) - preferably a fully free one
- a user named suda


Get install script from this repo 

`wget https://gitlab.com/hacklab01/suda/-/raw/main/suda-installer.sh`


Run the script (the script clones this repo into $HOME/suda-git/)

`bash suda-installer.sh`

