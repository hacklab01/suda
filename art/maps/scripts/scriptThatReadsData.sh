#!/bin/bash

input="$HOME/maps/scripts/data/theMouseRecord.txt"

while IFS= read -r line
do
  echo "$line"
  sleep 1s
done < "$input"
